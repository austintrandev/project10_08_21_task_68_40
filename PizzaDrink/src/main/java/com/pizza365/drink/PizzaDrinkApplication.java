package com.pizza365.drink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaDrinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaDrinkApplication.class, args);
	}

}
