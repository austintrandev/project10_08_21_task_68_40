package com.pizza365.drink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza365.drink.model.*;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}
